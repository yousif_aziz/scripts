#!/bin/bash
#!/bin/sh
# reset jetbrains ide evals

OS_NAME=$(uname -s)
JB_PRODUCTS="IntelliJIdea CLion PhpStorm GoLand PyCharm WebStorm Rider DataGrip RubyMine AppCode"

for PRD in $JB_PRODUCTS; do
	rm -rf ~/.java/.userPrefs/
	rm -rf ~/.config/JetBrains/${PRD}*
	rm -rf ~/.config/JetBrains/${PRD}*
done
